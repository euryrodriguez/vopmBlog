function mostrar_dialogo(id,titulo)
{
    $(id).dialog({
        title: titulo,
        autoOpen:true, // no abrir automáticamente
        resizable: true, //permite cambiar el tamaño
        width:700,
        height:400, // altura
        modal: true, //capa principal, fondo opaco
        buttons: { //crear botón de cerrar
            "Cerrar": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    $(id).dialog({ hide: { effect: "explode", duration: 500 } });
}

function Desplazarse(seccion) {
    $("body,html").animate({
        scrollTop: $(seccion).offset().top - 100
    }, 600);

}

