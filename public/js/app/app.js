var app = angular.module('VopmBlog', ['ngRoute','ngResource']);

app.config(function($routeProvider)
{
    $routeProvider
        .when('/', {
            controller:"MainController",
            templateUrl:'templates/index.html'
        })
        .when('/users', {
            controller:"MainController",
            templateUrl:'templates/users.html'
        })
        .when('/post/:id',{
            controller:"PostController",
            templateUrl:"templates/post.html"
        })
        .when('/posts/new',{
            controller:"NewPostController",
            templateUrl:"templates/newPost.html"
        })
        .when('/post/edit/:id',{
            controller:"PostController",
            templateUrl:"templates/newPost.html"
        })
        .when('/contacto',{
            controller:"MainController",
            templateUrl:"templates/contacto.html"
        })
        .otherwise('/');
});