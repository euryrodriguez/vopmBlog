var app = angular.module('VopmBlog');

app.directive('myAutocomplete',function()
{
    return function link(scope,element,attrs)
    {
        $(element).autocomplete({
            source:scope.$eval(attrs.myAutocomplete),
            select:function(ev,ui)
            {
                ev.preventDefault();
                
                if(ui.item){

                    scope.OptionSelected(ui.item.value);

                }
            },
            focus:function(ev,ui)
            {
                ev.preventDefault();
                $(this).val(ui.item.label);
                console.log(ui.item.label);
            }
        });
    };

})