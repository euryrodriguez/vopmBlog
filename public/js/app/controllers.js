var app = angular.module('VopmBlog');

app.controller('MainController',function($scope,$resource,$http,PostResource)
{
    User = $resource('https://jsonplaceholder.typicode.com/users/:id',{id:"@id"});

    $scope.posts = PostResource.query();
    $scope.users = User.query();

    //query()  -> GET/POST  -> un arreglo de posts

    $scope.removePost = function(post)
    {
        /*******************************************************************************************/
        PostResource.delete({id:post.id},function(data){    console.log(data);   });
        $scope.posts = $scope.posts.filter(function(element){   return element.id !== post.id;   });
        /*******************************************************************************************/
    }

    //Rellenar arreglo para el buscador
    $scope.res = [];
    $http.get('https://jsonplaceholder.typicode.com/posts/')
        .success(function(data)
        {
            $scope.posts = PostResource.query();

            for(var i = data.length -1 ;i>=0;i--)
            {

                var repo=data[i];
                $scope.res.push(repo.title);
                //alert(repo.name);

            };
        })
        .error(function(err)
        {
            console.log(err);
        })
    
    $scope.OptionSelected = function (data) {
        window.location = '#/post/'+data;
    }
    /*****************************************************************************/
});

app.controller('PostController',function($scope,$routeParams,PostResource,$location)
{
    $scope.title= "Editar Post";
    $scope.post = PostResource.get({id:$routeParams.id});
    //Get -> un objeto Json
    $scope.savePost = function()
    {
        PostResource.update({id:$scope.post.id},{data:$scope.post},function(data){ console.log(data); });
        $location.path("/post/"+$scope.post.id)

    }
});

app.controller('NewPostController',function($scope,$routeParams,PostResource,$location)
{
    $scope.post = {};
    $scope.title = "Nuevo Articulo";

    $scope.savePost = function()
    {
        $scope.post = {};
        $scope.title="Crear Post";
        $scope.savePost = function()
        {
            PostResource.save({data:$scope.post},function(data){ console.log(data);
                $location.path("/");
            });
        }
    }
});