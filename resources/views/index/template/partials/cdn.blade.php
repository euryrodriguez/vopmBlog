@if(App::environment('local'))

    <script src="{{ asset('plugins/angularjs/angular.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery/jquery.2.1.3.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

@else
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="{{ asset('plugins/Jquery-UI/jquery-ui.js') }}"></script>

@endif

<script src="{{ asset('plugins/angularjs/angular-resource.js') }}"></script>
<script src="{{ asset('plugins/angularjs/angular-route.min.js') }}"></script>
<script src="{{ asset('plugins/Jquery-UI/jquery-ui.js') }}"></script>
<script src="{{ asset('js/funciones.js') }}"></script>
<script src="{{ asset('js/slide.js') }}"></script>
<script src="{{ asset('js/app/app.js') }}"></script>
<script src="{{ asset('js/app/controllers.js') }}"></script>
<script src="{{ asset('js/app/services.js') }}"></script>
<script src="{{ asset('js/app/directivas.js') }}"></script>


@if(App::environment('local'))

    <link rel="stylesheet" href="{{ asset('plugins/fonts/font-waesome/css/font-awesome.min.css') }}">
    <!--<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrapSwatchLumen.min.css') }}">
    <link href="{{ asset('plugins/Jquery-UI/jquery-ui.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css')}}" />


    @else
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="{{ asset('plugins/Jquery-UI/jquery-ui.min.css')}}" rel="stylesheet" />

@endif