<nav class="navbar navbar-inverse col-xs-12" id="menu">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">Vopm</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#/">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                        Articulos
                        <span class="sr-only">(current)</span></a></li>
                <li><a href="#/posts/new">
                        <span class="glyphicon glyphicon-plus"></span>
                        Agregar
                    </a></li>
                <li><a href="#/users">
                        <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                        Usuarios
                    </a></li>
                <li><a href="#/contacto">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                        Contacto</a></li>

            </ul>
           @include('index.template.partials.buscador')
        </div>
    </div>
</nav>