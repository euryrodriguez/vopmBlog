<form class="navbar-form navbar-right" role="search">
    <div class="form-group">
        <div class="ui-widget">
            <input type="text" id="tags" my-autocomplete="res" class="form-control" placeholder="Buscar Articulos">
        </div>
    </div>
    <button type="submit" class="btn btn-warning">
        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
    </button>
</form>
