<meta http-equiv="content-type" content="text/html;" charset="utf-8"  />
<meta name="description" content="descripcion de la pagina" />
<meta name="keywords" content="">
<meta name="author" content="" charset="utf-8" />
<meta name="viewport" content="width-device-width, initial-scale=1, maximum-scale=1"/>
<meta name="robots" content="index, follow" />

<!--SEO-->
<link rel="canonical" href="http://www..com/" />
<link rel="author" href=""/>
<link rel="publisher" href=""/>

<meta property="og:title" content="Eury Rodríguez"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="es_ES"/>
<meta property="og:url" content="http://www..com/"/>
<meta property="og:description" content=""/>
<meta property="og:image" content=""/>
<meta property="og:site_name" content="www.euryrodriguez.com"/>
<meta property="article:publisher" content="https://www.facebook.com/euryrodriguez08"/>

<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@eury08"/>
<meta name="twitter:title" content="Eury Rodriguez"/>
<meta name="twitter:description" content=""/>
<meta name="twitter:creator" content="@eury08"/>
<meta name="twitter:card" content="summary"/>
<meta name="twitter:image:src" content=""/>
<meta name="twitter:domain" content=""/>

<!--SEO-->
@include('index.template.partials.cdn')

<link href="{{ asset('favicon.ico') }}" type="image/x-icon" rel="shortcut icon"/>

<style>

    body{
        cursor: url({{ asset('plugins/mouse/left_ptr.cur') }}),auto !important;

    }
    body a:hover
    {
        cursor: url({{ asset('plugins/mouse/pointing_hand.cur') }}),pointer !important;

    }

    #Textbuscar
    {

        cursor:url({{ asset('plugins/mouse/Text.cur') }}), text !important;
    }
    #boton_buscar
    {
        cursor: url({{ asset('plugins/mouse/pointing_hand.cur') }}),pointer !important;

    }

</style>