<footer class="col-xs-12">


    <div class="contenedor">
        <p class="copy">VOPM &copy; <?php echo date('Y');  ?></p>
        <div class="sociales">
            <a href="#" title="Facebook">
                <i class="fa fa-facebook-official"  id="fb" aria-hidden="true"></i>
            </a>
            <a href="#" title="Twitter">
                <i class="fa fa-twitter" id="tw" aria-hidden="true"></i>
            </a>
            <a href="#" title="Instagram">
                <i class="fa fa-instagram" id="it" aria-hidden="true"></i>
            </a>
            <!-- <a href="#">
                 <i class="fa fa-youtube" id="yb" aria-hidden="true"></i>
             </a>-->

        </div>
    </div>

    <div class="pie_menu">
        <ul>

            @for($i=0;$i<5;$i++)

                <li>
                    <a href="#">
                    Category {{ $i }}
                    </a>
                </li>
           @endfor


        </ul>

    </div>


</footer>
