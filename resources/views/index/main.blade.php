<!doctype html>
<html ng-app="VopmBlog">
<head>
    <meta charset="UTF-8">
    @include('index.template.partials.metadatos')
    <title>Vopm Blog</title>
</head>
<body ng-controller="MainController">
<main id="main" class="row">
    <div id="top"></div>
    @include('index.template.partials.header')
    <section id="content" class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
        @yield('contenido')
    </section>
    <section id="aside" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
       @yield('aside')
    </section>
</main>

@include('index.template.partials.footer')
</body>
</html>