@extends('index.main')
@section('contenido')
<div class="container-fluid">
 <div id="contenedor-post">
     <br>
     <div class="panel panel-primary">
         <div class="panel-heading">
             <h3 class="panel-title" id="articulos_title">&Uacute;ltimas Publicaciones &nbsp; </h3>
         </div>
         <div class="panel-body">

             <div ng-view></div>


         </div>
         <div class="panel-footer">
             <div id="paginacion">
                 <ul class="pagination">
                     <li class="disabled"><a href="#">&laquo;</a></li>
                     <li class="active"><a href="#">1</a></li>
                     <li><a href="#">2</a></li>
                     <li><a href="#">3</a></li>
                     <li><a href="#">4</a></li>
                     <li><a href="#">5</a></li>
                     <li><a href="#">&raquo;</a></li>
                 </ul>
             </div>
         </div>
     </div>
 </div>
</div>
@endsection
@section('aside')
@include('index.template.partials.aside')
@endsection